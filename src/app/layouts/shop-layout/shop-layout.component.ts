import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { of, Observable } from 'rxjs';
import * as selectors from 'src/app/redux/selectors'
import * as actions from 'src/app/redux/actions'

import { map, take } from 'rxjs/operators';
import { AppShopState } from '../../redux/app.reducer';


@Component({
  selector: 'app-admin-layout',
  templateUrl: './shop-layout.component.html',
  styleUrls: ['./shop-layout.component.scss']
})
export class ShopLayoutComponent implements OnInit, AfterViewInit {
  isCollapsed$: Observable<boolean> = of(false);

  constructor(private store: Store<AppShopState>) { }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.isCollapsed$ = this.store.select(selectors.getCollapse);
      
    }, 0)
    
  }

  ngOnInit(): void {
    this.store.dispatch(actions.loadingGetAllCarts());
    this.store.dispatch(actions.LoadinGetAllProducts());
   
  }

  updateSidebar(event) {
  }


}
