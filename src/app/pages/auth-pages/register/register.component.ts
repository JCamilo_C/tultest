import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../redux/app.reducer';
import * as actions from 'src/app/redux/actions'
import * as selectors from 'src/app/redux/selectors'
import { Observable, of } from 'rxjs';
import { AlertModel } from '../../../shared/models/alert.model';
import { User } from 'src/app/shared/models/user.model';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  loadingAuth$: Observable<boolean> = of(false)
  registerForm!: FormGroup;

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.loadingAuth$ = this.store.select(selectors.getLoadingAuth);
    this.initForm();
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.registerForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  initForm() {
    this.registerForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)]),
      repeatPassword: new FormControl('', [Validators.required]),
    });

    this.registerForm.get('repeatPassword').setValidators([Validators.required, this.confirmationValidator])
  }

  submitForm() {
    if (this.registerForm.valid) {
      let user: Partial<User> = this.registerForm.value;
      this.store.dispatch(actions.LoadingRegister({user}))
    } else {
      const alert: AlertModel = {
        duration: 3500,
        message: 'Debes ingresar todos los campos',
        type: 'warning'
      }
      this.store.dispatch(actions.toggleAlert({alert}))
    }
  }

}
