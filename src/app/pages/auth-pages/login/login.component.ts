import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AppState } from 'src/app/redux/app.reducer';
import * as selectors from 'src/app/redux/selectors';
import * as actions from 'src/app/redux/actions';
import { AlertModel } from '../../../shared/models/alert.model';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;
  loadingAuth$: Observable<boolean> = of(false)

  constructor(private store: Store<AppState>) { }

  ngAfterViewInit(): void {
    //
    this.store.dispatch(actions.LoadingAutoLogin())
  }

  ngOnInit(): void {
    
    this.loadingAuth$ = this.store.select(selectors.getLoadingAuth);
    this.initForm();
  }

  initForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    })
  }

  submitForm() {
    if  (this.loginForm.valid) {
      const {email, password} = this.loginForm.value;
      this.store.dispatch(actions.LoadingLogin({email, password}))
    } else {
      const alert: AlertModel = {
        duration: 3500,
        message: 'Debes ingresar todos los campos',
        type: 'warning'
      }
      this.store.dispatch(actions.toggleAlert({alert}))
    }
  }

}
