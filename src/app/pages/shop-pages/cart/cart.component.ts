import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppShopState } from 'src/app/redux/app.reducer';
import { Observable, of } from 'rxjs';
import { Cart } from 'src/app/shared/models/cart.model';
import * as selectors from 'src/app/redux/selectors'


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, AfterViewInit {
  currentCart$:  Observable<{cart: Cart, uid: string}> = of(null);
  orders$:  Observable<Cart[]> = of([]);
  loadingCart$:  Observable<boolean> = of(false);

  constructor(private store: Store<AppShopState>) { }
  
  ngAfterViewInit(): void {}

  ngOnInit(): void {
    this.currentCart$ = this.store.select(selectors.getDefaultCart);
    this.orders$ = this.store.select(selectors.getOrders);
    this.loadingCart$ = this.store.select(selectors.loadingCart);
  }

  createCart() {

  }

}
