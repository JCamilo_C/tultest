import { NgModule } from '@angular/core';
import {  CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShopRoutingModule } from './shop-pages.routes';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';


import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpinModule } from 'ng-zorro-antd/spin';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffects } from '../../redux/effects/product.effects';
import { ComponentsModule } from '../../components/components.module';
import { ShopStateKey } from '../../../config/feature.keys';
import { appShopReducers } from '../../redux/app.reducer';
import { CartEffects } from '../../redux/effects/cart.effects';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    DashboardComponent,
    CartComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    ShopRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    NzGridModule,
    ComponentsModule,
    NzEmptyModule,
    NzDividerModule,
    NzButtonModule,
    NzSpinModule,
    StoreModule.forFeature(ShopStateKey, appShopReducers),
    EffectsModule.forFeature([ProductEffects, CartEffects]),
  ]
})
export class ShopPagesModule { }
