import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopLayoutComponent } from './layouts/shop-layout/shop-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { 
    path: '', 
    pathMatch: 'full', 
    redirectTo: '/auth/login' 
  },
  { 
    path: 'shop',
    component: ShopLayoutComponent,
    canLoad: [AuthGuard],
    loadChildren: () => import('./pages/shop-pages/shop-pages.module').then(m => m.ShopPagesModule) 
  },
  { 
    path: 'auth',
    component: AuthLayoutComponent,
    loadChildren: () => import('./pages/auth-pages/auth-pages.module').then(m => m.AuthPagesModule) 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
