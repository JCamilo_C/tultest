import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from '../redux/app.reducer';
import * as actions from 'src/app/redux/actions'
import * as selectors from 'src/app/redux/selectors'
import { map } from 'rxjs/operators';
import { AlertModel } from '../shared/models/alert.model';
import { AngularFireAuth } from '@angular/fire/auth';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private afAuth: AngularFireAuth, private router: Router, private store: Store<AppState>) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(selectors.isAuthenticate).pipe( map( auth => {
      if (!auth) {
        const alert: AlertModel = {
          duration: 2500,
          message: `Debes iniciar sesion primero`,
          type: 'danger'
        }
        this.store.dispatch(actions.toggleAlert({alert}));
        this.router.navigate(['/auth/login'])
        return auth;
      }
      return auth
    }));
  }
  
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(selectors.isAuthenticate).pipe( map( auth => {
      if (!auth) {
        const alert: AlertModel = {
          duration: 2500,
          message: `Debes iniciar sesion primero`,
          type: 'danger'
        }
        this.store.dispatch(actions.toggleAlert({alert}));
        this.router.navigate(['/auth/login'])
      }
      return auth
    }));
  }
}
