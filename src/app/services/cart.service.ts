import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { ResponseModel } from '../shared/models/response.model';
import { COLLECTION_CART, COLLECTION_CART_ITEM } from '../../config/collections.config';
import { map, switchMap } from 'rxjs/operators';
import { Cart, CartItems } from '../shared/models/cart.model';
import { Product } from '../shared/models/product.model';
import * as firebase from 'firebase/app'
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private afs: AngularFirestore) { }

  getAllCartsByUid(uid: string): Observable<ResponseModel<Cart[]>> {
    const docRef = this.afs.collection<Cart>(COLLECTION_CART, ref => ref.where('uid', '==', uid))
    return docRef.get().pipe(
      map( (doc) => {
        if (!doc.empty) {
          const rm: ResponseModel<Cart[]> = {
            success: true,
            response: doc.docs.map( (cart)  => ({...(cart.data() as Cart), id: cart.id, cartItems: []}) , [])
          }
          return rm
        } else {
          const rm: ResponseModel<Cart[]> = {
            success: false,
            message: 'No se han registrado productos aun :('
          }
          return rm;
        }
      })
    )
  }

  getAllProductItems(cartId: string): Observable<ResponseModel<CartItems[]>> {
    const docRef = this.afs.collection(COLLECTION_CART).doc(cartId).collection<CartItems>(COLLECTION_CART_ITEM)
    return docRef.get().pipe(
      map( (doc) => {
        if (!doc.empty) {
          const rm: ResponseModel<CartItems[]> = {
            success: true,
            response: doc.docs.map( (cart)  => ({...(cart.data() as CartItems), id: cart.id}) , [])
          }
          return rm
        } else {
          const rm: ResponseModel<CartItems[]> = {
            success: false,
            message: 'No se han registrado productos aun :('
          }
          return rm;
        }
      })
    )
  }

  addToCart(cartId: string, product: Product): Observable<CartItems> {
    const cartItemId = this.afs.createId();
    const cartProductRef: AngularFirestoreDocument<CartItems> = this.afs.doc(`${COLLECTION_CART}/${cartId}/${COLLECTION_CART_ITEM}/${cartItemId}`); 
    const cartItemData: CartItems = {
      cartId: cartId,
      productId: product.id,
      productName: product.name,
      productSku: product.sku,
      quanty: 1
    }
    return from(cartProductRef.set(cartItemData, {merge: true}).then( () => cartItemData).catch(() => null))
  }

  updateQuanty(cartId: string, productId: string, quanty: number): Observable<ResponseModel<CartItems>> {
    return this.afs.collection<CartItems>(`${COLLECTION_CART}/${cartId}/${COLLECTION_CART_ITEM}`, ref => ref.where('productId', '==', productId).limit(1)).get().pipe( 
      map(doc => {
        const rm: ResponseModel<CartItems> = {
          success: true
        }
        if (!doc.empty) {
          doc.forEach( d => d.ref.update({quanty}))
          rm.response = doc.docs.length > 0 ? {...doc.docs[0].data() as CartItems, quanty} : null
        } else {
          rm.success = false;
          rm.message = 'No se encontro el producto'
        }
        return rm
      })
    )

  }


  createOrder(cartId: string): Observable<ResponseModel<Cart>> {
    return this.afs.doc<Cart>(`${COLLECTION_CART}/${cartId}`).get().pipe( 
      map(doc => {
        const rm: ResponseModel<Cart> = {
          success: true
        }
        if (doc.exists) {
          const cart: Cart = {...doc.data()};
          cart.order = uuid.v4();
          cart.status = 'completed'
          cart.orderTime = firebase.default.firestore.Timestamp.now();
          doc.ref.update({
            ...cart
          })
          rm.response = cart;
        } else {
          rm.success = false;
          rm.message = 'No se encontro el carrito'
        }
        return rm
      })
    )

  }

  deleteItemFromCart(cartId: string, productId: string): Observable<ResponseModel<void>> {
    return this.afs.collection<CartItems>(`${COLLECTION_CART}/${cartId}/${COLLECTION_CART_ITEM}`, ref => ref.where('productId', '==', productId)).get().pipe( map(doc => {
      if (!doc.empty) {
        doc.forEach( r => r.ref.delete())
      }
      const rm : ResponseModel<void> = {
        success: true
      }
      return rm
    }));
  }

  createCart(uid: string, product: Product = null ): Observable<Cart> {
    const cartId = this.afs.createId();
    const cartRef: AngularFirestoreDocument<Partial<Cart>> = this.afs.doc(`${COLLECTION_CART}/${cartId}`);
    const cartData: Partial<Cart> = {
      id: cartId,
      status: 'pending',
      uid
    }
    if(product) {
      const cartSub$ = from(cartRef.set(cartData, {merge: true}).then( () => {
        const cartItemId = this.afs.createId();
        const cartProductRef: AngularFirestoreDocument<CartItems> = this.afs.doc(`${COLLECTION_CART}/${cartId}/${COLLECTION_CART_ITEM}/${cartItemId}`); 
        const cartItemData: CartItems = {
          cartId: cartId,
          productId: product.id,
          productName: product.name,
          productSku: product.sku,
          quanty: 1
        }
        cartProductRef.set(cartItemData, {merge: true})
        cartData.cartItems = [cartItemData]
        return cartData
      }).catch(() => null))
      return cartSub$
    } else {
      return from(cartRef.set(cartData, {merge: true}).then( () => cartData).catch(() => null))
    }
  }
  
}
