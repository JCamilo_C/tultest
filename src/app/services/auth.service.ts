import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { from, Observable, of } from 'rxjs';
import { ResponseModel } from '../shared/models/response.model';
import { User } from '../shared/models/user.model';
import { map, switchMap } from 'rxjs/operators';
import { COLLECTION_USERS } from 'src/config/collections.config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth, public ngZone: NgZone) { }

  Login(email: string, password: string): Observable<ResponseModel<User>> {
    return from(this.afAuth.signInWithEmailAndPassword(email, password).then((result) => {
      return {success: true, response: result.user.uid} 
    }).catch((error) => {
      return {success: false, error: error.message}
    })).pipe(
      switchMap( (response: {success: boolean, error?: string,  response?: string}) => {
        if (!response.success) {
          const rm: ResponseModel<User> = {
            success: false,
            message: response.error
          }
          return of(rm)
        }
        return this.getUserById(response.response)
      })
    )
  }

  Register(data: Partial<User>):  Observable<ResponseModel<User>> {
    return from(this.afAuth.createUserWithEmailAndPassword(data.email, data.password).then((result) => {
      let registerUser =  this.SetUserData(result.user, data);
      const rm: ResponseModel<User> = {
        success: true,
        response: registerUser
      }
      return rm;
    }).catch(err => {
      const rm: ResponseModel<User> = {
        success: false,
        message: err.message
      }
      return rm;
    }))
  }

  isAuthenticate():  Observable<ResponseModel<User>> {
    return this.afAuth.authState.pipe(
      switchMap( user => {
        if (user) {
          return this.getUserById(user.uid)
        } else {
          const rm: ResponseModel<User> = {
            success: false,
            message: 'No esta autenticado'
          }
          return of(rm);
        }
      })
    )
  }

  SignOut() : Observable<ResponseModel<void>> {
    return from(this.afAuth.signOut().then(() =>  ({success: true})).catch(err => ({success: false, message: err})))
  }


  /* Agregar usuario a bd */
  SetUserData(user: firebase.default.User, data: Partial<User>): User {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`${COLLECTION_USERS}/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      firstName: data.firstName,
      lastName: data.lastName,
      photoURL: user.photoURL
    }
    userRef.set(userData, {
      merge: true
    })
    return userData
  }

  getUserById(uid: string): Observable<ResponseModel<User>> {
    var docRef = this.afs.collection(COLLECTION_USERS).doc<User>(uid);
    const user$ = docRef.get().pipe(
      map( (doc) => {
        if (doc.exists) {
          const rm: ResponseModel<User> = {
            success: true,
            response: doc.data()
          }
          return rm
        } else {
          const rm: ResponseModel<User> = {
            success: false,
            message: 'Usuario no encontrado'
          }
          return rm;
        }
      })
    )
    return user$
  }
}
