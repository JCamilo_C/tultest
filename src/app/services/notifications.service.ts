import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AppState } from '../redux/app.reducer';
import { AlertModel } from '../shared/models/alert.model';
import * as actions from 'src/app/redux/actions';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private notificationService:  NzNotificationService, private store: Store<AppState>) { }

  launchNotificatio(alert: AlertModel) {
    switch (alert.type) {
      case 'danger':
        this.notificationService.error("A ocurrido un error", alert.message, { nzDuration: alert.duration}).onClose.subscribe( close => {
          if (!close) {
            this.store.dispatch(actions.toggleAlert({alert: null}))
          }
        })
        break;
      case 'success':
        this.notificationService.success("!Bien¡", alert.message, { nzDuration: alert.duration}).onClose.subscribe( close => {
          if (!close) {
            this.store.dispatch(actions.toggleAlert({alert: null}))
          }
        })
        break; 
      case 'info':
        this.notificationService.info("Escucha...", alert.message, { nzDuration: alert.duration}).onClose.subscribe( close => {
          if (!close) {
            this.store.dispatch(actions.toggleAlert({alert: null}))
          }
        })
        break;
      default:
        this.notificationService.warning("Espera...", alert.message, { nzDuration: alert.duration}).onClose.subscribe( close => {
          if (!close) {
            this.store.dispatch(actions.toggleAlert({alert: null}))
          }
        })
        break;
    }
  }
}
