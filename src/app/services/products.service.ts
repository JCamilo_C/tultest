import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { COLLECTION_PRODUCT } from 'src/config/collections.config';
import { Product } from '../shared/models/product.model';
import { ResponseModel } from '../shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  herramientas: string[] = [
    'Destornillador',
    'Pala',
    'Martillo',
    'Clavos',
    'Escalera',
    'Alicate',
    'Pinzas',
    'Brocas',
    'Cizalla',
    'Llave',
    'Tenaza',
    'Cinta metrica',
    'Sierra'
  ]

  constructor(private afs: AngularFirestore) { }

  getAllProducts(): Observable<ResponseModel<Product[]>> {
    const docRef = this.afs.collection<Product>(COLLECTION_PRODUCT)
    return docRef.valueChanges().pipe(
      map( (doc) => {
        if (doc.length > 0) {
          const rm: ResponseModel<Product[]> = {
            success: true,
            response: doc
          }
          return rm
        } else {
          const rm: ResponseModel<Product[]> = {
            success: false,
            message: 'No se han registrado productos aun :('
          }
          return rm;
        }
      })
    )
  }

  createProduct() {
    const productId = this.afs.createId();
    const productRef: AngularFirestoreDocument<Product> = this.afs.doc(`${COLLECTION_PRODUCT}/${productId}`);
    const productData: Product = {
      id: productId,
      name: this.herramientas[Math.floor((Math.random()*this.herramientas.length))],
      description: "Suspendisse eget ex eget dolor dignissim venenatis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla massa felis, interdum luctus posuere in, venenatis suscipit massa. Aliquam dolor justo, rutrum laoreet tortor vitae, luctus mollis quam. Donec aliquet ut lorem eu porta. In nulla ex, fermentum eget eros nec, ultrices dictum odio. Suspendisse auctor ligula nec sapien venenatis, in placerat ipsum consequat. In id pretium est. Maecenas sed massa blandit, malesuada metus vitae, vehicula felis. Nulla id elit iaculis, consequat odio rutrum, posuere nibh. Cras facilisis malesuada turpis vitae hendrerit. Sed nec est magna. Phasellus rhoncus.",
      sku: Math.floor(Math.random() * 1000000000).toString()
    }

    return from(productRef.set(productData, {merge: true}).then( () => productData).catch(() => null))
  }
  
}
