import { createReducer, on, Action } from '@ngrx/store';
import { AlertModel } from 'src/app/shared/models/alert.model';

import * as uiActions from '../actions/ui.actions'
export interface UiState {
    collapse: boolean,
    alert: AlertModel
}

const initState: UiState = {
    collapse: false,
    alert: null
}

const reducer = createReducer(
    initState,
    on(uiActions.toggleCollapseUiLoading, (state, ) => ({...state, collapse: !state.collapse})),
    on(uiActions.toggleAlert, (state, {alert}) => ({...state, alert})),
)

export function uiReducer(state = initState, action: Action) {
    return reducer(state, action)
}