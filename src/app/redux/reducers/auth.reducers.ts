import { Action, createReducer, on } from '@ngrx/store';
import { User } from '../../shared/models/user.model';
import * as authActions from 'src/app/redux/actions'

export interface AuthState {
    loading: boolean;
    user: User;
    error: any;
}

const initState: AuthState = {
    loading: false,
    user: null,
    error: null
}

const reducer = createReducer<AuthState>(
    initState,
    on(
        authActions.LoadingLogin,
        authActions.LoadingSignOut,
        authActions.LoadingAutoLogin,
        authActions.LoadingRegister, (state) => ({ ...state, loading: true}) ),
    on(authActions.LoginSuccesfull, (state, {user}) => ({ 
        ...state, 
        loading: false, 
        user
    })),
    on(authActions.FailAutoLogin,
       authActions.RegisterSuccesfull, (state) => ({ ...state, 
        loading: false
    })),
    on(authActions.signOutSuccesfull, (state) => ({ ...state, 
        loading: false, 
        user: null
    })),
    on(authActions.FailAuthAction, (state, {error}) => ({ ...state, 
        loading: false, 
        error
    }))
)


export function authReducer(state=initState, action: Action) {
    return reducer(state, action);
}