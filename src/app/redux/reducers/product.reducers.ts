import { Product } from "src/app/shared/models/product.model";
import { createReducer, on, Action } from '@ngrx/store';
import * as productActions from '../actions/products.actions'

export interface ProductState {
    loading: boolean,
    products: Product[],
    error: string
}

const initState: ProductState = {
    loading: true,
    products: [],
    error: null
}

const Reducer = createReducer(
    initState,
    on(productActions.LoadinGetAllProducts, (state) => ({...state, loading: true})),
    on(productActions.getAllProducts, (state, {products}) => ({...state, products, loading: false})),
    on(productActions.FailProductActions, (state, {error}) => ({...state, error, loading: false})),
)

export function productReducer(state=initState, action: Action) {
    return Reducer(state, action);
}