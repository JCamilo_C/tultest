
import { createReducer, on, Action } from '@ngrx/store';
import * as CartActions from '../actions/cart.actions';
import { Cart } from '../../shared/models/cart.model';
import { loadingCreateOrder, createNewOrder } from '../actions/cart.actions';

export interface CartState {
    loading: boolean;
    items: Cart[];
    TotalQuanty: number;
    defaultCart: string;
    error: string;
}

const initState: CartState = {
    loading: true,
    TotalQuanty: 0,
    items: [

    ],
    error: null,
    defaultCart: null
};

const Reducer = createReducer(
    initState,
    on(CartActions.loadingGetAllCarts,
        CartActions.loadingAddToCart,
        CartActions.loadingGetProductItems,
        CartActions.loadingCreateOrder,
        CartActions.LoadingToogleQuantyProduct,
        CartActions.LoadingRemoveItemFromCart, (state) => ({...state, loading: true})),
    on(CartActions.GetAllCarts, (state, {carts}) => ({
        ...state, 
        items: carts, 
        defaultCart: carts.some(cart => cart.status === 'pending') ? carts.find(cart => cart.status === 'pending').id : null,  
        loading: false
    })),
    on(CartActions.GetProductItems, (state, {cartId,item}) => ({...state, items: [ ...state.items.map(  cart => {
        if (cartId === cart.id) {
            cart =  {...cart, cartItems: [...cart.cartItems, ...item]}
        }
        return cart
    })] , loading: false})),
    on(CartActions.failLoadignCarts, (state, {message}) => ({...state, error: message , loading: false})),
    on(CartActions.AddToCart, (state, {cartId, item}) => {
        return {
            ...state,
            items: [ ...state.items.map( c => {
                if (c.id === cartId) {
                    c = {...c, cartItems:  [...c.cartItems, item] }
                }
                return {...c};
            })],
            TotalQuanty: state.TotalQuanty + 1,
            loading: false
        }
    }),
    on(CartActions.createNewOrder, (state, {order}) => ({
        ...state, 
        items: state.items.map( cart => {
            if (cart.id === order.id) {
                cart = {...cart, ...order}
            }

            return cart
        }),
        defaultCart: null,
        TotalQuanty: 0,
        loading: false
    })),
    on(CartActions.AddNewCart, (state, {cart}) => ({...state, items: [...state.items, cart], TotalQuanty: state.TotalQuanty + 1, defaultCart: cart.id,  loading: false})),
    on(CartActions.SetQuantyProduct, (state, {cartId, productId, quanty}) => ({
        ...state,
        TotalQuanty: quanty,
        items: [...state.items.map( item => {
            if (item.id === cartId) {
                item.cartItems = [...item.cartItems.map( product => {
                    if (product.productId === productId) {
                        product.quanty = quanty;
                    }
                    return product
                })]
            }
            return item;
        })],
        loading: false
    })),
    on(CartActions.ToogleQuantyProduct, (state, {cartId, productId, quanty}) => ({
        ...state,
        loading: false,
        TotalQuanty: state.TotalQuanty + quanty,
        items: [...state.items.map( item => {
            if (item.id === cartId) {
                item = {...item, cartItems: item.cartItems.map( product => {
                    if (product.productId === productId) {
                        product = {...product, quanty: quanty};
                    }
                    return {...product}
                })}
            }
            return {...item};
        })]
    })),
    on(CartActions.UpdateTotalQuanty, (state) => ({
        ...state,
        loading: false,
        TotalQuanty: state.items.length > 0 ? state.items.reduce( (p, c) =>  {
            let totalQuantyItemPerCart = 0;
            if (c.status === 'pending') totalQuantyItemPerCart = c.cartItems.reduce( (total, cartItem) => total + cartItem.quanty, 0);
            return p + totalQuantyItemPerCart;
        }, 0) : 0,
    })),
    on(CartActions.RemoveCart, (state, {cartId}) => ({
        ...state,
        loading: false,
        items: [...state.items.filter( i => i.id !== cartId)],
    })),
    on(CartActions.RemoveItemFromCart, (state, {cartId, productId}) => ({
        ...state,
        loading: false,
        items: [...state.items.map( i => {
            if (i.id === cartId) {
                const filterProducts = i.cartItems.filter(p => p.productId !== productId);
                if (filterProducts.length > 0) {
                    i = {...i, cartItems:  [...filterProducts]}
                } else {
                    i = {...i, cartItems: []}
                }
            }
            return i
        })]
    })),
    on(CartActions.RemoveAllItemsFromCart, (state) => ({
        ...state,
        items: [],
        TotalQuanty: 0,
        Total: 0,
        loading: false
    }))
);

export function cartReducer(state = initState, action: Action): CartState {
    return Reducer(state, action);
}
