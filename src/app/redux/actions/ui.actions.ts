import { createAction, props } from '@ngrx/store';
import { AlertModel } from 'src/app/shared/models/alert.model';

export const toggleAlert = createAction(
    '[UI] Toggle Alert',
    props<{ alert: AlertModel}>()
)

export const toggleCollapseUiLoading = createAction(
    '[UI] Toggle Collapse'
)