import { createAction, props } from '@ngrx/store';
import { Cart, CartItems } from 'src/app/shared/models/cart.model';
import { Product } from 'src/app/shared/models/product.model';


export const loadingGetAllCarts = createAction(
    `[CART] Loading all carts`
);

export const GetAllCarts = createAction(
    `[CART]  Get all carts`,
    props<{ carts: Cart[]}>()
);

export const loadingCreateOrder = createAction(
    `[CART] Loading create new order`,
    props<{ cartId: string}>()
);

export const createNewOrder = createAction(
    `[CART]  Create new order`,
    props<{ order: Cart}>()
);

export const AddNewCart = createAction(
    `[CART]  add new carts`,
    props<{ cart: Cart}>()
);

export const failLoadignCarts = createAction(
    `[CART]  Fail get all carts`,
    props<{ message: string}>()
);


export const loadingGetProductItems = createAction(
    `[CART]  loading get product items cart`,
    props<{ cartId: string }>()
);


export const GetProductItems = createAction(
    `[CART] Get cart items`,
    props<{ cartId: string, item: CartItems[] }>()
);

export const loadingAddToCart = createAction(
    `[CART]  loading add cart product cart`,
    props<{ item: Product }>()
);


export const AddToCart = createAction(
    `[CART] add new product`,
    props<{ cartId: string, item: CartItems }>()
);

export const LoadingToogleQuantyProduct = createAction(
    `[CART]  Loading Toogle quanty product`,
    props<{ cartId: string, productId: string, quanty: number }>()
);

export const ToogleQuantyProduct = createAction(
    `[CART]  Toogle quanty product`,
    props<{ cartId: string, productId: string, quanty: number }>()
);

export const SetQuantyProduct = createAction(
    `[CART]  Set Toogle quanty product`,
    props<{ cartId: string, productId: string, quanty: number }>()
);

export const UpdateTotalQuanty = createAction(
    `[CART]  Update total quanty`,
);

export const RemoveCart = createAction(
    `[CART]  Remove cart state from cart`,
    props<{ cartId: string }>()
);

export const LoadingRemoveItemFromCart = createAction(
    `[CART]  Loading Remove product from cart`,
    props<{ productId: string }>()
);

export const RemoveItemFromCart = createAction(
    `[CART]  Remove product from cart`,
    props<{ cartId: string, productId: string }>()
);

export const RemoveAllItemsFromCart = createAction(
    `[CART]  Remove all products from cart`,
);
