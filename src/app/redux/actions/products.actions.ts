import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/shared/models/product.model';


export const LoadinGetAllProducts = createAction(
    '[PRODUCTS] Loading get all products'
)

export const getAllProducts = createAction(
    '[PRODUCTS] Get all products',
    props<{products: Product[]}>()
)

export const createProduct = createAction(
    '[PRODUCTS] Create product',
)


export const FailProductActions = createAction(
    '[PRODUCTS] Fail  product action',
    props<{error: string}>()
)