import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/shared/models/user.model';

export const LoadingAutoLogin = createAction(
    '[Auth] Loading auto login'
)

export const FailAutoLogin = createAction(
    '[Auth] Loading auto login'
)

export const LoadingLogin = createAction(
    '[Auth] Loading login',
    props<{email: string, password: string}>()
)

export const LoginSuccesfull = createAction(
    '[Auth] Login Access',
    props<{user: User}>()
)

export const LoadingRegister = createAction(
    '[Auth] Loading Register',
    props<{user: Partial<User>}>()
)

export const RegisterSuccesfull = createAction(
    '[Auth] Register Successfully'
)

export const LoadingSignOut = createAction(
    '[Auth] Loading sign out'
)

export const signOutSuccesfull = createAction(
    '[Auth] Sign out Successfully'
)


export const FailAuthAction = createAction(
    '[Auth] Error login',
    props<{error: string}>()
)

export const UpdateSession = createAction(
    '[Auth] update login',
    props<{authentication: User}>()
)