import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import * as reducers from 'src/app/redux/reducers'
import { localStorageSync } from 'ngrx-store-localstorage';
import { ProductState, productReducer } from './reducers/product.reducers';
import { ShopStateKey } from '../../config/feature.keys';
import { cartReducer, CartState } from './reducers/cart.reducers';

export interface AppState {
    ui: reducers.UiState;
    auth: reducers.AuthState;
}

export const appReducers: ActionReducerMap<AppState> = {
    ui: reducers.uiReducer,
    auth: reducers.authReducer,
};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({
        keys: ['auth', 'ui', {[ShopStateKey]: [{'cart': ['defaultCart', 'TotalQuanty', 'items']}]}],
        rehydrate: true
    })(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];


// ------ Shiop
export interface AppShopState extends AppState {
  products: ProductState,
  cart: CartState,
}

export const appShopReducers = {
  products: productReducer,
  cart: cartReducer
};