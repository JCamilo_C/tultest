import { Injectable } from "@angular/core";
import { CartService } from '../../services/cart.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as fromActions from '../actions'
import * as selectors from '../selectors'
import { Store } from "@ngrx/store";
import { AppShopState } from "../app.reducer";
import { mergeMap, switchMap, take, tap } from 'rxjs/operators';
import { CartItems, Cart } from '../../shared/models/cart.model';
import { from } from 'rxjs';


@Injectable()
export class CartEffects {

    constructor(private cartService: CartService, private actions: Actions, private store: Store<AppShopState>) {}

    loadingAddToCart$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.loadingAddToCart),
        switchMap( (action) => this.store.select(selectors.getDefaultCart).pipe(
            take(1),
            switchMap( cart => {
                if (cart.cart) {
                    return this.cartService.addToCart(cart.cart.id, action.item).pipe(
                        switchMap( response => {
                            let cartItem: CartItems = response as CartItems;
                            return [fromActions.AddToCart({ cartId: cart.cart.id, item: cartItem})]
                        })
                    )
                } else {
                    return this.cartService.createCart(cart.uid, action.item).pipe(
                        switchMap( response => {
                            if(!response) return [fromActions.failLoadignCarts({message: null})]
                            let newCart: Cart = response as Cart;
                            return[fromActions.AddNewCart({cart: newCart})]
                        })
                    )
                }
            })
        ))
    ))

    loadingProductCartById$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.loadingGetProductItems),
        mergeMap( action => this.cartService.getAllProductItems(action.cartId).pipe(
            switchMap( response => {
                if (!response.success) {
                    return [fromActions.failLoadignCarts({message: null})]
                }

                return [fromActions.GetProductItems({cartId: action.cartId, item: response.response})]
            })
        ))
    ))


    loadingDeleteItemById$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.LoadingRemoveItemFromCart),
        switchMap( action => this.store.select(selectors.getDefaultCart).pipe(
            take(1),
            switchMap( cart => this.cartService.deleteItemFromCart(cart.cart.id, action.productId).pipe(
                switchMap( response => {
                    if (!response.success) {
                        return [fromActions.failLoadignCarts({message: null})]
                    }
    
                    return [fromActions.RemoveItemFromCart({cartId: cart.cart.id, productId: action.productId})]
                })
            ))
        ))
    ))

    loadingUpdateQuantyById$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.LoadingToogleQuantyProduct),
        switchMap( action =>  this.cartService.updateQuanty(action.cartId, action.productId, action.quanty).pipe(
            switchMap( response => {
                if (!response.success) {
                    return [fromActions.failLoadignCarts({message: null})]
                }

                return [fromActions.ToogleQuantyProduct({cartId: action.cartId, productId: action.productId, quanty: action.quanty})]
            })
        ))
    ))

    loadingCreateOrder$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.loadingCreateOrder),
        switchMap( action =>  this.cartService.createOrder(action.cartId).pipe(
            switchMap( response => {
                if (!response.success) {
                    return [fromActions.failLoadignCarts({message: null})]
                }

                return [fromActions.createNewOrder({order: response.response})]
            })
        ))
    ))



    getAllCartByUid$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.loadingGetAllCarts),
        switchMap( () => this.store.select(selectors.getUid).pipe(
            take(1),
            switchMap( uid => this.cartService.getAllCartsByUid(uid).pipe(
                switchMap( response => {
                    if (!response.success) {
                        return [fromActions.GetAllCarts({carts: []}), fromActions.failLoadignCarts({message: null})]
                    }

                    let arrayActions = response.response.map(cart => fromActions.loadingGetProductItems({cartId: cart.id}))
                    
                    return [
                        ...arrayActions,
                        fromActions.GetAllCarts({carts: response.response}),
                    ]
                })
            ))
        ))
    ))

    updateTotalQuanty$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.GetProductItems, fromActions.ToogleQuantyProduct, fromActions.RemoveItemFromCart),
        switchMap( () => [fromActions.UpdateTotalQuanty()])
    ))

}