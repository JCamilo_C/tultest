import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { catchError, switchMap, tap } from 'rxjs/operators';
import * as fromActions from '../actions'
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AlertModel } from '../../shared/models/alert.model';
import { AuthService } from '../../services/auth.service';

@Injectable()
export class AuthEffect {

    constructor(private authService: AuthService, private actions: Actions, private router: Router) {}

    loadingAuth$ =  createEffect( () => this.actions.pipe(
        ofType(fromActions.LoadingLogin),
        switchMap( action => this.authService.Login(action.email, action.password).pipe(
            switchMap( response => {
                if (!response.success) {
                    const alert: AlertModel = {
                        duration: 3500,
                        message: response.message,
                        type: 'warning'
                    }
                    return [
                        fromActions.FailAuthAction({error: response.message}),
                        fromActions.toggleAlert({alert})
                    ]
                }
                const alert: AlertModel = {
                    duration: 2500,
                    message: `Bienvenid@ de nuevo ${response.response.firstName}`,
                    type: 'success'
                }
                return [
                    fromActions.LoginSuccesfull({user: response.response}),
                    fromActions.toggleAlert({alert})
                ]
            }),
            catchError( (err: HttpErrorResponse) => {
                const alert: AlertModel = {
                    duration: null,
                    message: err.message,
                    type: 'danger'
                }
                return [
                    fromActions.FailAuthAction({error: err.message}),
                    fromActions.toggleAlert({alert})
                ]
            })
        ))
    ))

    LoginSuccesfull$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.LoginSuccesfull),
        tap( () => {
            return this.router.navigate(['/shop/dashboard'])
        })
    ), {dispatch: false})

    loadingSignOut$ =  createEffect( () => this.actions.pipe(
        ofType(fromActions.LoadingSignOut),
        switchMap( () => this.authService.SignOut().pipe(
            switchMap( response => {
                if (!response.success) {
                    const alert: AlertModel = {
                        duration: 3500,
                        message: response.message,
                        type: 'warning'
                    }
                    return [
                        fromActions.FailAuthAction({error: response.message}),
                        fromActions.toggleAlert({alert})
                    ]
                }
                return [
                    fromActions.signOutSuccesfull()
                ]
            }),
            catchError( (err: HttpErrorResponse) => {
                const alert: AlertModel = {
                    duration: null,
                    message: err.message,
                    type: 'danger'
                }
                return [
                    fromActions.FailAuthAction({error: err.message}),
                    fromActions.toggleAlert({alert})
                ]
            })
        ))
    ))

    SignOutSuccess$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.signOutSuccesfull),
        tap( () => {
            return this.router.navigate(['/auth/login'])
        })
    ), {dispatch: false})

    loadingRegister$ =  createEffect( () => this.actions.pipe(
        ofType(fromActions.LoadingRegister),
        switchMap( action => this.authService.Register(action.user).pipe(
            switchMap( response => {
                if (!response.success) {
                    const alert: AlertModel = {
                        duration: 3500,
                        message: response.message,
                        type: 'warning'
                    }
                    return [
                        fromActions.FailAuthAction({error: response.message}),
                        fromActions.toggleAlert({alert})
                    ]  
                }
                return [
                    fromActions.RegisterSuccesfull(),
                    fromActions.LoadingLogin({email: response.response.email, password: action.user.password})
                ]
            }),
            catchError( (err: HttpErrorResponse) => {
                const alert: AlertModel = {
                    duration: 3500,
                    message: err.message,
                    type: 'danger'
                }
                return [
                    fromActions.FailAuthAction({error: err.message}),
                    fromActions.toggleAlert({alert})
                ]
            })
        ))
    ))


    loadingAutoLogin$ =  createEffect( () => this.actions.pipe(
        ofType(fromActions.LoadingAutoLogin),
        switchMap( () => this.authService.isAuthenticate().pipe(
            switchMap( response => {
                if (!response.success) {
                    return [fromActions.FailAutoLogin()]  
                }
                return [fromActions.LoginSuccesfull({user: response.response})]
            }),
            catchError( (err: HttpErrorResponse) => {
                const alert: AlertModel = {
                    duration: 3500,
                    message: err.message,
                    type: 'danger'
                }
                return [
                    fromActions.FailAuthAction({error: err.message}),
                    fromActions.toggleAlert({alert})
                ]
            })
        ))
    ))
    

}