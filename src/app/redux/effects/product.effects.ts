import { Injectable } from "@angular/core";
import { ProductsService } from '../../services/products.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as fromActions from '../actions'
import { switchMap, catchError, tap } from 'rxjs/operators';
import { AlertModel } from '../../shared/models/alert.model';

@Injectable()
export class ProductEffects {

    constructor(private productService: ProductsService, private actions: Actions) {}

    loadingGetAllProduct$ = createEffect(() => this.actions.pipe(
        ofType(fromActions.LoadinGetAllProducts),
        switchMap( action => this.productService.getAllProducts().pipe(
            switchMap( response => {
                if (!response.success) {
                    const alert: AlertModel = {
                        duration: 3500,
                        message: response.message,
                        type: 'warning'
                    }
                    return [
                        fromActions.FailProductActions({error: response.message}),
                        fromActions.toggleAlert({alert})
                    ]
                }
                return [
                    fromActions.getAllProducts({products: response.response}),
                ]
            }),
            catchError( err => {
                const alert: AlertModel = {
                    duration: 3500,
                    message: err.message,
                    type: 'warning'
                }
                return [
                    fromActions.FailProductActions({error: err.message}),
                    fromActions.toggleAlert({alert})
                ]
            })
        ))
    ))


    createProduct$ = createEffect(() => this.actions.pipe(
        ofType(fromActions.createProduct),
        tap( () => {
            this.productService.createProduct()
        })
    ), {dispatch: false})
}