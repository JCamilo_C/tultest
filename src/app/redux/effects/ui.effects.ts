import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import * as fromActions from '../actions'
import { NotificationsService } from '../../services/notifications.service';

@Injectable()
export class UiEffect {

    constructor(private notiService: NotificationsService, private actions: Actions) {}

    dispatchAlert$ = createEffect( () => this.actions.pipe(
        ofType(fromActions.toggleAlert),
        tap( action => {
            if (!action.alert) return
            return this.notiService.launchNotificatio(action.alert)
        })
    ), {dispatch: false})

}