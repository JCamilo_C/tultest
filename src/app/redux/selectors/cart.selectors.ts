import { AppState, AppShopState } from '../app.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartState } from '../reducers/cart.reducers';
import { ShopStateKey } from '../../../config/feature.keys';
import { AuthState } from '../reducers/auth.reducers';

const featureCartState= createFeatureSelector<AppShopState>(ShopStateKey);

const selectCartState = createSelector(
    featureCartState,
    (state: AppShopState) => state.cart
)

const selectUserState = (state: AppState) => state.auth

export const GetTotalQuantyByCart = createSelector(
    selectCartState,
    (state: CartState, props) => {
        if (state.items.length === 0) return 0;
        const item = state.items.find( cart => cart.id === props.id)
        if (item) return item.cartItems.reduce( (total, cartItem) => total + cartItem.quanty, 0);
        return 0;
    }
);

export const GetTotalQuantyInCart = createSelector(
    selectCartState,
    (state: CartState) => state.TotalQuanty
);



export const GetCartsAllCarts = createSelector(
    selectCartState,
    (state: CartState) => state.items
);

export const getDefaultCart = createSelector(
    selectCartState,
    selectUserState,
    (state: CartState, auth: AuthState) => {
        const uid = auth.user ? auth.user.uid : null;
        if (state.items.length >  0) {
            if (state.items.some( cart => cart.id === state.defaultCart)) {
                return {cart: state.items.find(cart => cart.id === state.defaultCart), uid}
            }
            else {
                return {cart: null, uid}
            }
        } else {
          return {cart: null, uid}  
        } 
    }
);


export const getOrders = createSelector(
    selectCartState,
    (state: CartState) =>  {
        if (state.items.length >  0) {
            return state.items.filter(cart  => cart.status === 'completed');
        } else {
            return []
        }
    });

export const getOrdersItemsByCartId = createSelector(
    selectCartState,
    (state: CartState, props) =>  {
        if (state.items.length >  0) {
            return state.items.find(cart  => cart.id === props.id).cartItems;
        } else {
            return []
        }
    });
    


export const loadingCart = createSelector(
    selectCartState,
    (state: CartState) => state.loading
);


export const ProductExistInCart = createSelector(
    selectCartState,
    (state: CartState, props) => {
        if (state.items.length > 0) {
            if (state.items.some( cart => cart.id === state.defaultCart)) return state.items.find(cart => cart.id === state.defaultCart).cartItems.some( product => product.productId === props.id)
            else return false
        } else {
            return false;
        }
    }
);



