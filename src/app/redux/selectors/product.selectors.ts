import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppShopState } from '../app.reducer';
import { ShopStateKey } from '../../../config/feature.keys';

const featureStoreSelector = createFeatureSelector<AppShopState>(ShopStateKey);

export const getProductLoading = createSelector(
    featureStoreSelector,
    (state: AppShopState) => state.products.loading
)

export const getAllProducts = createSelector(
    featureStoreSelector,
    (state: AppShopState) => state.products.products
)