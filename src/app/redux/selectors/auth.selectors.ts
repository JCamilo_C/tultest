import { createSelector } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { AuthState } from '../reducers/auth.reducers';


const SelectAuthState = (state: AppState) => state.auth;

export const getUid = createSelector(
    SelectAuthState,
    (auth: AuthState) => auth.user.uid
)

export const isAuthenticate = createSelector(
    SelectAuthState,
    (auth: AuthState) => auth.user ? true : false
)

export const getLoadingAuth = createSelector(
    SelectAuthState,
    (auth: AuthState) => auth.loading
)

export const getUserData = createSelector(
    SelectAuthState,
    (auth: AuthState) => auth.user
)