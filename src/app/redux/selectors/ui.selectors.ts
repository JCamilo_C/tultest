import { createSelector } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { UiState } from '../reducers/ui.reducers';


const SelectUiState = (state: AppState) => state.ui;

export const getAlert = createSelector(
    SelectUiState,
    (ui: UiState) => ui.alert
)

export const getCollapse = createSelector(
    SelectUiState,
    (ui: UiState) => ui.collapse
)