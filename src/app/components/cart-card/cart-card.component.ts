import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppShopState } from 'src/app/redux/app.reducer';
import * as actions  from 'src/app/redux/actions';
import { Cart } from '../../shared/models/cart.model';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AlertModel } from '../../shared/models/alert.model';

@Component({
  selector: 'app-cart-card',
  templateUrl: './cart-card.component.html',
  styleUrls: ['./cart-card.component.scss']
})
export class CartCardComponent implements OnInit {

  @Input() cart: Cart = null;
  public config: PerfectScrollbarConfigInterface = {};

  constructor(private store: Store<AppShopState>) { }

  ngOnInit(): void {  }

  updateQuanty(productId: string, quanty: number) {
    this.store.dispatch(actions.LoadingToogleQuantyProduct({cartId: this.cart.id, productId, quanty}))
  }

  deleteProduct( productId: string) {
    this.store.dispatch(actions.LoadingRemoveItemFromCart({productId}))
  }

  createOrder(cartId: string) {
    if (this.cart.cartItems.length > 0) {
      this.store.dispatch(actions.loadingCreateOrder({cartId}))
      return
    }
    
    const alert: AlertModel = {
      duration: 2500,
      message: "Debe agregar al menos un producto para realizar la orden",
      type: 'danger'
    }
    this.store.dispatch(actions.toggleAlert({alert}))
    return
    
  }

}
