import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { Product } from 'src/app/shared/models/product.model';
import * as selectors from 'src/app/redux/selectors'
import * as actions from 'src/app/redux/actions'
import { AppShopState } from '../../redux/app.reducer';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  loadingProduct$: Observable<boolean> = of(true)
  loadingCart$: Observable<boolean> = of(true)
  products$: Observable<Product[]> = of([])
  testCards!: string[];
  showMoreMessage: boolean = false;
  showMoreMessageIndex: number = null;

  constructor(private store: Store<AppShopState>) { }

  ngOnInit(): void {
    this.testCards = Array(10).fill('x');
    this.loadingProduct$ = this.store.select(selectors.getProductLoading)
    this.loadingCart$ = this.store.select(selectors.loadingCart)
    this.products$ = this.store.select(selectors.getAllProducts)
  }

  toggleMessage(i): void {
    this.showMoreMessage = !this.showMoreMessage;
    this.showMoreMessageIndex = i;
    return;
  }

  addToCart(product: Product) {
    this.store.dispatch(actions.loadingAddToCart({item: product}))
  }

  deleteProduct(productId: string) {
    this.store.dispatch(actions.LoadingRemoveItemFromCart({productId}))
  }

  elemenExisteInCart(productId: string): Observable<boolean> {
    return this.store.select(selectors.ProductExistInCart, {id: productId})
  }

}
