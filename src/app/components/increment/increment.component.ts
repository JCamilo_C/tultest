import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppShopState } from 'src/app/redux/app.reducer';
import * as actions from 'src/app/redux/actions'
import { AlertModel } from '../../shared/models/alert.model';

@Component({
  selector: 'app-increment',
  templateUrl: './increment.component.html',
  styleUrls: ['./increment.component.scss']
})
export class IncrementComponent implements OnInit {
  @Output() changeQuanty: EventEmitter<number> = new EventEmitter()
  @Input() quanty: number = 0;

  constructor(private store: Store<AppShopState>) { }

  ngOnInit(): void {
  }

  add() {
    this.quanty += 1;
    this.changeQuanty.emit(this.quanty)
  }

  decrement() {
    if ((this.quanty - 1) <= 0 ) {
      const alert: AlertModel = {
        duration: 2500,
        message: "No puedes colocar cantidades menores a cero",
        type: 'danger'
      }
      this.store.dispatch(actions.toggleAlert({alert}))
      return
    }
    this.quanty -= 1;
    this.changeQuanty.emit(this.quanty)
  }

}
