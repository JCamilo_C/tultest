import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card/product-card.component';

import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzEmptyModule } from 'ng-zorro-antd/empty';

import { IconsProviderModule } from '../icons/icons-provider.module';
import { PipesModule } from '../pipes/pipes.module';
import { CartCardComponent } from './cart-card/cart-card.component';
import { IncrementComponent } from './increment/increment.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [
    ProductCardComponent,
    CartCardComponent,
    IncrementComponent
  ],
  exports: [
    ProductCardComponent,
    CartCardComponent
  ],
  imports: [
    CommonModule,
    NzCardModule,
    NzGridModule,
    NzAvatarModule,
    NzTagModule,
    IconsProviderModule,
    PipesModule,
    NzButtonModule,
    NzPopconfirmModule,
    NzListModule,
    NzSkeletonModule,
    NzDescriptionsModule,
    NzBadgeModule,
    PerfectScrollbarModule,
    NzEmptyModule
  ]
})
export class ComponentsModule { }
