import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../redux/app.reducer';
import { Observable, of } from 'rxjs';
import * as selectors from 'src/app/redux/selectors'
import * as actions from 'src/app/redux/actions';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  isCollapsed$: Observable<boolean> = of(false);

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.isCollapsed$ = this.store.select(selectors.getCollapse)
  }

  addProduct() {
    this.store.dispatch(actions.createProduct());
  }

}
