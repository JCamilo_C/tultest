export interface AlertModel {
    message: string;
    duration: number;
    type: 'danger' | 'info' | 'success' | 'warning';
}