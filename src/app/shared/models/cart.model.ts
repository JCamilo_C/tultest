import * as firebase from 'firebase/app';

export interface CartItems  {
    productId: string;
    productName: string;
    productSku: string;
    cartId: string;
    quanty: number
}

export interface Cart {
    id: string;
    status: 'pending' | 'completed';
    order?: string;
    orderTime?: firebase.default.firestore.Timestamp;
    orderTimeCopy?: Date;
    uid: string;
    cartItems: CartItems[];
} 