import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AppState, AppShopState } from '../../redux/app.reducer';
import * as actions from 'src/app/redux/actions';
import * as selectors from 'src/app/redux/selectors';
import { User } from '../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isCollapsed$: Observable<boolean> = of(false);
  user$: Observable<User> = of(null);
  totalInCart$: Observable<number> = of(0);

  constructor(private store: Store<AppShopState>) { }

  ngOnInit(): void {
    this.isCollapsed$ = this.store.select(selectors.getCollapse)
    this.user$ = this.store.select(selectors.getUserData)
    this.totalInCart$ = this.store.select(selectors.GetTotalQuantyInCart)
  }

  toggleMenu() {
    this.store.dispatch(actions.toggleCollapseUiLoading())
  }

  SignOut() {
    this.store.dispatch(actions.LoadingSignOut())
  }

}
