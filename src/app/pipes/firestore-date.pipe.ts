import { formatDate } from '@angular/common';
import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import * as firebase from 'firebase/app'

@Pipe({
  name: 'firestorePipe'
})
export class FirestoreDatePipe implements PipeTransform {

  constructor(@Inject(LOCALE_ID) private locale: string) {
  }

  transform(timestamp: firebase.default.firestore.Timestamp, format?: string): string {
      if (!timestamp?.toDate) {
          return null;
      }
      return formatDate(timestamp.toDate(), format || 'medium', this.locale);
  }
}