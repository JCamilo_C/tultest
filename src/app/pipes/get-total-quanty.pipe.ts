import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppShopState } from '../redux/app.reducer';
import * as selectors from 'src/app/redux/selectors'

@Pipe({
  name: 'getTotalQuanty'
})
export class GetTotalQuantyPipe implements PipeTransform {

  constructor(private store: Store<AppShopState>) {}

  transform(cartId: string): Observable<number> {
    return this.store.select(selectors.GetTotalQuantyByCart, {id: cartId});
  }

}
