import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LimitToPipe } from './limit-to.pipe';
import { GetTotalQuantyPipe } from './get-total-quanty.pipe';
import { FirestoreDatePipe } from './firestore-date.pipe';



@NgModule({
  declarations: [
    LimitToPipe,
    GetTotalQuantyPipe,
    FirestoreDatePipe
  ],
  exports: [
    LimitToPipe,
    GetTotalQuantyPipe,
    FirestoreDatePipe,
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
